const jwt = require('jsonwebtoken');

module.exports = (request, response, next) => {
    const token = request.header('Authorization');
    if (!token) return response.status(401).send({message: 'Access Denied'});

    try {
        const decoded = jwt.verify(token, process.env.TOKEN_SECRET);
        request.user = {
            user_id: decoded.user_id,
            password: decoded.password
        };
    } catch (err) {
        return response.status(401).send({message: 'Authentication Faild'});
    }

    return next();
};
