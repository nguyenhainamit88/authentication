const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const userSchema = new mongoose.Schema({
    user_id: {
        type: String,
        required: true,
        min: 6,
        max: 20
    },
    password: {
        type: String,
        required: true,
        min: 8,
        max: 20
    },
    nickname: {
        type: String,
        // required: true,
        min: 6,
        
        max: 255
    },
    comment: {
        type: String,
        // required: true,
        min: 6,
        max: 255
    }
});

userSchema.methods.generateAuthToken = function() {
    // Create token following format: 
    // Authorization header: Basic <base64Encoded {user_id} + ':' + {password}>
    const dataToken = {
        user_id: this.user_id, 
        colon: ":", 
        password: this.password
    };
    const token = jwt.sign(dataToken, process.env.TOKEN_SECRET, { expiresIn: 60 * 60 * 24 });

    return token;
}

const User = mongoose.model('User', userSchema);

module.exports = {
    User
};
