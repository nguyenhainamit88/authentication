const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const verifyToken = require('./../middlewares/verifyToken');
const router = express.Router();
const { User } = require('./../models/User');
const { registerValidator } = require('./../validations/auth');

router.post('/register', async (request, response) => {
    const { error } = registerValidator(request.body);

    if (error) return response.status(400).send(error.details[0].message);

    const checkUserExist = await User.findOne({ user_id: request.body.user_id });

    if (checkUserExist) return response.status(400).send('User is exist');

    try {
        const salt = await bcrypt.genSalt(10);
        const hashPassword = await bcrypt.hash(request.body.password, salt);
    
        const user = new User({
            user_id: request.body.user_id,
            password: hashPassword,
            nickname: request.body.nickname,
            comment: request.body.comment
        });

        const newUser = await user.save();
        return response.send(newUser);
    } catch (err) {
        return response.status(400).send(err);
    }
});

// Consider to not use sync in this feature
router.post('/login', async (request, response) => {
    const user = await User.findOne({ user_id: request.body.user_id});

    if (!user) return response.status(422).send({message: 'Username or Password is not correct'});

    const checkPassword = await bcrypt.compare(request.body.password, user.password);
    if (!checkPassword) return response.status(422).send({ message: 'Username or Password is not correct' });

    const token = user.generateAuthToken();

    return response.send({
        message: 'Login successfully',
        token
    });
})

module.exports = router;
