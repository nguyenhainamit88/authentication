const express = require('express');
const jwt = require('jsonwebtoken');
const verifyToken = require('./../middlewares/verifyToken');
const { User } = require('./../models/User');
const { validatorUpdateUser } = require('./../validations/user');
const router = express.Router();

router.get('/:user_id', verifyToken, (request, response) => {
    const { user_id } = request.params;

    User.findOne({ user_id })
        .then(user => {
            if ( user === null ) {
                return response.status(404).send({
                    "message": "No User found"
                });
            }
    
            let user_id = user.user_id;
            let nickname = user.nickname;
    
            //When nickname is not configured, set the same value as user_id .
            if (nickname === "") {
                nickname = user_id;
            }
    
            return response.send({
                "message": "User details by user_id",
                "user": {
                    user_id,
                    nickname
                }
            });
        })
        .catch( err => {
            return response.status(404).send({
                "message": "No User found",
                err
            });
        });
});

router.patch('/:user_id', verifyToken, (request, response) => {
    const own_user_id = request.user.user_id;
    const { error } = validatorUpdateUser(request.body);
    const { user_id } = request.params;

    //When nickname is empty, it returns to default value ( user_id )
    let nickname = request.body.nickname;
    if (request.body.nickname) {
        nickname = request.body.nickname;
    } else {
        nickname = user_id;
    }

    // Failure (Trying to change user_id or password )
    if ( request.body.user_id !== undefined || request.body.password !== undefined ) {
        return response.status(400).send({
            "message": "User updation failed",
            "cause": "not updatable user_id and password"
        });
    }

    // Show validate error when validator found issues
    if (error) return response.status(422).send(error.details[0].message);

    // Failure (If neither nickname or comment is selected)
    if ( request.body.nickname === "" && request.body.comment === "" ) {
        return response.status(400).send({
            "message": "User updation failed",
            "cause": "required nickname or comment"
        });
    }

    // Failure (When the selected user id was authorized by a different user)
    if (own_user_id !== user_id) {
        return response.status(403).send({
            "message": "No Permission for Update"
        });
    }

    // Update user after validator
    User.findOneAndUpdate({ user_id }, {
        nickname: nickname,
        comment: request.body.comment
   }, { new: true })
    .then(user => {
        return response.send({
            "message": "User successfully updated",
            "recipe": {
                nickname: user.nickname,
                comment: user.comment
            }
        });
    })
   .catch( err => {
        return response.status(404).send({
            "message": "No User found"
        });
    });
});

router.post('/close', verifyToken, (request, response) => {
    const user_id = request.user.user_id;

    User.findOneAndRemove({ user_id }, function () {
        return response.send({
            "message": "Account and user successfully removed"
        });
    });
});

module.exports = router;
