# Authentication_API_Server
# Project name : Authentication_API_Server

# How to config .env
 - create mongodb (mongodb+srv://feedforce:feedforce123@cluster0.quwaq.mongodb.net/feedforce_db)
 - create .env
 - PORT=6000
 - TOKEN_SECRET=your token
 - DB_CONNECT=link to connect mongodb

# How to run project
 - npm install
 - npm start

# How to run test
 - npm run test

# MongoDB
- https://cloud.mongodb.com/

# Heroku CLI
 - `heroku login`
 - `heroku git:clone -a feedforce-authentication`
 - `git add .`
 - `git commit -am "make it better"`
 - `git push heroku master`

# Explain technical decisions
- Choose NodeJs and MongoDb for quickly start (I have known PHP and NodeJs. In this case, I prefer NodeJs because of its lightness, and can serve a lot of requests/s)
- Choose NodeJs because of its famous, save time to develop and debug
- Choose NodeJs for Feedforce App should be better instead of PHP

# Point out one or more future improvements
- Using Docker
- Apply caches
- Improve performance better
- Handle error better
- Monitor project by sentry.io
- Apply MVC, Module design pattern

# Quick test
## Domain: https://feedforce-authentication.herokuapp.com
    1. POST /api/auth/register -> Create a user account
    2. POST /api/auth/login -> Get token
    3. GET /api/users/:user_id -> Get Info User
    4. PATCH /api/users/:user_id -> Update User
    5. POST /api/users/close -> Delete User