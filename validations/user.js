const Joi = require('joi');

module.exports.validatorUpdateUser = (data) => {
    const rule = Joi.object({
        nickname: Joi.string().allow('').max(30),
        comment: Joi.string().allow('').max(100)
    })

    return rule.validate(data);
};