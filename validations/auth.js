const Joi = require('joi');

const registerValidator = (data) => {
    const rule = Joi.object({
        nickname: Joi.string().min(3).max(30).required(),
        user_id: Joi.string().min(6).max(20).required(),
        password: Joi.string().min(8).max(20).pattern(new RegExp('^[a-zA-Z0-9]{6,20}$')).required(),
        comment: Joi.string().min(3).max(100)
    })

    return rule.validate(data);
}

module.exports.registerValidator = registerValidator;