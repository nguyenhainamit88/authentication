const request = require('supertest');
const { User } = require('../../models/User');
const mongoose = require('mongoose');

describe('/api/auth', () => {
    let server;
    let token, user_id, password, hashPassword, nickname, comment;
    const bcrypt = require('bcryptjs');

    beforeEach(async () => {
        server = require('../../bin/www'); 
    });

    afterEach(async () => { 
        await server.close(); 
    }); 

    describe('POST /api/auth/register', () => {
        beforeEach(async () => {
            user_id = "Test_NamNguyen_User";
            password = "12345678";
            nickname = "Test_NamNguyen_Nickname";
            comment = "Have a good day!!!!"
            const salt = await bcrypt.genSalt(10);
            hashPassword = await bcrypt.hash(password, salt);
            const user = new User({
                user_id,
                hashPassword,
                nickname,
                comment
            });
            token = user.generateAuthToken();
        });
    
        afterEach(() => {
            User.findOneAndRemove({ user_id }).exec();
        }); 

        const exec = async () => {
            return await request(server)
                .post('/api/auth/register')
                .set('Authentication', token)
                .send({
                    user_id,
                    password,
                    nickname,
                    comment
                });
        }

        it('should return 400 if user_id and password are empty', async () => {
            token = "";
            user_id = "";
            password = "";
            const req = await exec();
            expect(req.status).toBe(400);
        });

        it('should return 400 if user_id has less than 6 characters', async () => {
            token = "";
            user_id = "abc";
            // password = "";
            const req = await exec();
            expect(req.status).toBe(400);
        });

        it('should return 400 if user_id has over 20 characters', async () => {
            token = "";
            user_id = "aaaaaaaaaaaaaaaaaaaaa";
            // password = "";
            const req = await exec();
            expect(req.status).toBe(400);
        });

        it('should return 400 if database has already same user_id is used', async () => {
            token = "";
            const user = new User({
                user_id,
                password,
                nickname,
                comment
            });
            await user.save();

            const req = await exec();
            expect(req.status).toBe(400);
        });

        it('should return 200 if user_id and password are valid', async () => {
            token = "";
            const req = await exec();
            expect(req.status).toBe(200);
        });
    });

    describe('POST /api/auth/login', () => {
        beforeEach(async () => {
            user_id = "Test_NamNguyen_User";
            password = "12345678";
            nickname = "Test_NamNguyen_Nickname";
            comment = "Have a good day!!!!"
            const salt = await bcrypt.genSalt(10);
            hashPassword = await bcrypt.hash(password, salt);
    
            token = new User({
                user_id,
                hashPassword,
                nickname,
                comment
            }).generateAuthToken();

            const user = new User({
                user_id,
                password: hashPassword,
                nickname,
                comment
            });
            await user.save();
        });
        
        afterEach(async () => {
            await User.findOneAndRemove({ user_id }).exec();
        }); 

        const exec = async () => {
            return await request(server)
                .post('/api/auth/login')
                .set('Authentication', token)
                .send({
                    user_id,
                    password
                });
        }

        it('should return 422 if user_id is not correct', async () => {
            token = "";
            const temp = user_id;
            user_id = "abc";
            const req = await exec();
            user_id = temp;
            expect(req.status).toBe(422);
        });

        it('should return 422 if password is not correct', async () => {
            token = "";
            password = "abc";
            const req = await exec();
            expect(req.status).toBe(422);
        });

        it('should return 200 if user_id and password are correct', async () => {
            const req = await exec();
            expect(req.status).toBe(200);
        });
    });
});