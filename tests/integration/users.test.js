const request = require('supertest');
const { User } = require('../../models/User');
const mongoose = require('mongoose');

describe('/api/users', () => {
    let server;
    beforeEach(() => {
        server = require('../../bin/www'); 
    });

    afterEach(async () => { 
        await server.close(); 
    }); 

    describe('GET /api/users/:user_id', () => {
        let token, user, user_id, password, nickname, comment;

        beforeEach(async () => {
            user_id = "Test_NamNguyen_User";
            password = "12345678";
            nickname = "Test_NamNguyen_Nickname";
            comment = "Have a good day!!!!"
    
            user = new User({
                user_id,
                password,
                nickname,
                comment
            });
    
            await user.save();
            
            token = user.generateAuthToken();
        });

        afterEach(async () => { 
            await user.remove();
        }); 

        const exec = async () => {
            return await request(server)
                .get('/api/users/' + user_id)
                .set('Authorization', token);
        }

        it('should return 401 if no token', async () => {
            token = "";
            const res = await exec();
            expect(res.status).toBe(401);
        });

        it('should return 404 if no User found', async () => {
            user_id = "no_user";
            const res = await exec();
            expect(res.status).toBe(404);
        });

        it('should return 200 if User found', async () => {
            const res = await exec();
            expect(res.status).toBe(200);
        });

        it('when nickname is not configured, set the same value as user_id', async () => {
            const res = await exec();
            expect(res.status).toBe(200);
        });
    });

    describe('PATCH /api/users/:user_id', () => {
        let token, user, user_id, password, nickname, comment;

        beforeEach(async () => {
            user_id = "Test_NamNguyen_User";
            password = "12345678";
            nickname = "Test_NamNguyen_Nickname";
            comment = "Have a good day!!!!"
    
            user = new User({
                user_id,
                password,
                nickname,
                comment
            });
    
            await user.save();
            
            token = user.generateAuthToken();
        });

        afterEach(async () => { 
            await user.remove();
        }); 

        const exec = async () => {
            return await request(server)
                .patch('/api/users/' + user_id)
                .set('Authorization', token)
                .send({
                    nickname,
                    comment 
                });
        }

        it('should return 422 if nickname over than 30 characters', async () => {
            nickname = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa32";
            const res = await exec();
            expect(res.status).toBe(422);
        });

        it('should return 422 if comment over than 100 characters', async () => {
            comment = "aaaaaaaaaabbbbbbbbbbaaaaaaaaaabbbbbbbbbbaaaaaaaaaabbbbbbbbbbaaaaaaaaaabbbbbbbbbbaaaaaaaaaabbbbbbbbbb101";
            const res = await exec();
            expect(res.status).toBe(422);
        });

        it('should return 200 and nickname equal user_id if nickname is empty', async () => {
            nickname = "";
            const res = await exec();
            const dataResponse = JSON.parse(res.text);
            expect(dataResponse.recipe.nickname).toBe(user_id);
            expect(res.status).toBe(200);
        });

        it('should return 200 and clear comment if it is empty', async () => {
            comment = "";
            const res = await exec();
            const dataResponse = JSON.parse(res.text);
            expect(dataResponse.recipe.comment).toBe(comment);
            expect(res.status).toBe(200);
        });

        it('should return 400 because of all nickname and comment are empty', async () => {
            nickname = "";
            comment = "";
            const res = await exec();
            expect(res.status).toBe(400);
        });

        it('should return 400 because of trying to change user_id or password', async () => {
            const res = await request(server)
                .patch('/api/users/' + user_id)
                .set('Authorization', token)
                .send({
                    user_id: "hackeruser",
                    password: "hackroinha",
                    nickname,
                    comment
                });
            expect(res.status).toBe(400);
        });

        it('should return 403 because of no permission for Update', async () => {
            const res = await request(server)
                .patch('/api/users/hackeruser')
                .set('Authorization', token)
                .send({
                    nickname,
                    comment
                });
            expect(res.status).toBe(403);
        });
    });

    describe('GET /api/users/close', () => {
        let token, user, user_id, password, nickname, comment;

        beforeEach(async () => {
            user_id = "Test_NamNguyen_User";
            password = "12345678";
            nickname = "Test_NamNguyen_Nickname";
            comment = "Have a good day!!!!"
    
            user = new User({
                user_id,
                password,
                nickname,
                comment
            });
    
            await user.save();
            
            token = user.generateAuthToken();
        });

        afterEach(async () => { 
            await user.remove();
        }); 

        const exec = async () => {
            return await request(server)
                .post('/api/users/close')
                .set('Authorization', token);
        }

        it('should return 401 if no token', async () => {
            token = "";
            const res = await exec();
            expect(res.status).toBe(401);
        });

        it('should return 200 if token is vaild', async () => {
            const res = await exec();
            expect(res.status).toBe(200);
        });

    });
});