const { User } = require('../../../models/User');
const verifyToken = require('../../../middlewares/verifyToken');
const dotenv = require('dotenv');
dotenv.config();

describe('verifyToken middleware', () => {
    let token, user_id, password;

    beforeEach(() => {
        user_id = "TestUser";
        password = "12345678";
        token = new User({
            user_id, 
            password
        }).generateAuthToken();
    });
    
    const mockRequest = () => {
        return {
            header: jest.fn().mockReturnValue(token)
        }
    };
    const mockResponse = () => {
        const res = {};
        res.status = jest.fn().mockReturnValue(res);
        res.send = jest.fn().mockReturnValue(res);
        return res;
    };
    const next = () => jest.fn();

    it('should populate req.user with the payload of a vaild JW', () => {
        const mockedNext = next();
        const mockedReq = mockRequest();
        const mockedRes = mockResponse();

        verifyToken(mockedReq, mockedRes, mockedNext);
        
        expect(mockedReq.user).toMatchObject({
            user_id, 
            password
        });
    });

    it('should return 401 if token is empty', () => {
        token = "";
        const mockedNext = next();
        const mockedReq = mockRequest();
        const mockedRes = mockResponse();

        verifyToken(mockedReq, mockedRes, mockedNext);
        
        expect(mockedRes.status).toHaveBeenCalledWith(401);
    });

    it('should return 401 if token is invaild', () => {
        token = "abc";
        const mockedNext = next();
        const mockedReq = mockRequest();
        const mockedRes = mockResponse();

        verifyToken(mockedReq, mockedRes, mockedNext);
        
        expect(mockedRes.status).toHaveBeenCalledWith(401);
    });
});