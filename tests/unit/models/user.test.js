const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();
const { User } = require('../../../models/User');

describe('user.generateAuthToken', () => {
    it('should return a valid JWT', () => {
        const payload = {
            user_id: "TestUser", 
            colon: ":",
            password: "12345678"
        };
        const user = new User(payload);
        const token = user.generateAuthToken();
        const decoded = jwt.verify(token, process.env.TOKEN_SECRET);
        expect(decoded).toMatchObject(payload);
    });
});